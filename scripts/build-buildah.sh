#!/bin/sh

set -e

# In case $TAG is unset
TAG="${TAG:-}"

if [ -z "${TAG}" ]; then
    TAG="dev-branch${TAG_SUFFIX:-}"

    if [ "${CI_COMMIT_REF_NAME}" = "${CI_DEFAULT_BRANCH}" ]; then
        TAG="dev-${CI_DEFAULT_BRANCH}${TAG_SUFFIX:-}"
    fi

    if [ -n "${CI_COMMIT_TAG}" ]; then
        TAG="${CI_COMMIT_TAG}${TAG_SUFFIX:-}"
    fi
fi

# Prepare the default registry
cat <<EOF>registries.conf
[registries.search]
registries = ['docker.io']
EOF

BUILDAH_ARGS="--registries-conf registries.conf ${BUILDAH_ARGS} --squash --file ${CI_PROJECT_DIR}/Dockerfile --omit-history"

if [ "${ENABLE_GITLAB_REGISTRY}" = "true" ]; then
    BUILDAH_ARGS="${BUILDAH_ARGS} --tag=${CI_REGISTRY_IMAGE}:${TAG}"
fi

if [ -n "${DOCKERHUB_REPO_NAME}" ] && [ -n "${DOCKERHUB_PASSWORD}" ]; then
    BUILDAH_ARGS="${BUILDAH_ARGS} --tag=docker.io/${DOCKERHUB_REPO_PREFIX}/${DOCKERHUB_REPO_NAME}:${TAG}"
fi

if [ -n "${GITHUB_USERNAME}" ] && [ -n "${GITHUB_STATUS_TOKEN}" ] && [ -n "${GITHUB_REPO_NAME}" ]; then
    BUILDAH_ARGS="${BUILDAH_ARGS} --tag=ghcr.io/${GITHUB_REPO_NAME}:${TAG}"
fi

if [ "${ENABLE_CACHE}" = "true" ]; then
    if [ -n "${GITHUB_USERNAME}" ] && [ -n "${GITHUB_STATUS_TOKEN}" ] && [ -n "${GITHUB_REPO_NAME}" ]; then
        BUILDAH_ARGS="${BUILDAH_ARGS} --cache-from ghcr.io/${GITHUB_REPO_NAME}"
        BUILDAH_ARGS="${BUILDAH_ARGS} --cache-to ghcr.io/${GITHUB_REPO_NAME}"
    elif [ -n "${DOCKERHUB_REPO_NAME}" ] && [ -n "${DOCKERHUB_PASSWORD}" ]; then
        BUILDAH_ARGS="${BUILDAH_ARGS} --cache-from docker.io/${DOCKERHUB_REPO_PREFIX}/${DOCKERHUB_REPO_NAME}"
        BUILDAH_ARGS="${BUILDAH_ARGS} --cache-to docker.io/${DOCKERHUB_REPO_PREFIX}/${DOCKERHUB_REPO_NAME}"
    else
        BUILDAH_ARGS="${BUILDAH_ARGS} --cache-from ${CI_REGISTRY_IMAGE}"
        BUILDAH_ARGS="${BUILDAH_ARGS} --cache-to ${CI_REGISTRY_IMAGE}"
    fi
else
    BUILDAH_ARGS="${BUILDAH_ARGS} --no-cache"
fi

# For visiblity, this is split here on multiple stand-alone lines
BUILDAH_ARGS="${BUILDAH_ARGS} --build-arg CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME}"
BUILDAH_ARGS="${BUILDAH_ARGS} --build-arg CI_PIPELINE_ID=${CI_PIPELINE_ID}"
BUILDAH_ARGS="${BUILDAH_ARGS} --build-arg CUSTOM_VERSION=${CUSTOM_VERSION:-none}"

if [ -n "${CI_COMMIT_TAG}" ]; then
    BUILDAH_ARGS="${BUILDAH_ARGS} --build-arg LATEST=true"
fi

if [ ! "${ADDITIONAL_BUILDAH_ARGS:-}x" = "x" ]; then
    BUILDAH_ARGS="${BUILDAH_ARGS} ${ADDITIONAL_BUILDAH_ARGS}"
fi

set -x
# shellcheck disable=SC2086
buildah build ${BUILDAH_ARGS}
buildah images

if [ "${ENABLE_GITLAB_REGISTRY}" = "true" ]; then
    buildah push --authfile "/kaniko/.docker/config.json" --rm \
        "${CI_REGISTRY_IMAGE}:${TAG}"
fi

if [ -n "${DOCKERHUB_REPO_NAME}" ] && [ -n "${DOCKERHUB_PASSWORD}" ]; then
    buildah push --authfile "/kaniko/.docker/config.json" --rm \
        "docker.io/${DOCKERHUB_REPO_PREFIX}/${DOCKERHUB_REPO_NAME}:${TAG}"
fi

if [ -n "${GITHUB_USERNAME}" ] && [ -n "${GITHUB_STATUS_TOKEN}" ] && [ -n "${GITHUB_REPO_NAME}" ]; then
    buildah push --authfile "/kaniko/.docker/config.json" --rm \
        "ghcr.io/${GITHUB_REPO_NAME}:${TAG}"
fi
