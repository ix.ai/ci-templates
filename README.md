# ci-templates

The templates for the gitlab-ci jobs for ix.ai

## Projects

The following ix.ai projects are using these templates:

* ix.ai/cioban>
* ix.ai/alertmanager-telegram-bot>
* ix.ai/crypto-exporter>
* ix.ai/blockchain-exporter>
* ix.ai/coinbase-exporter>
* ix.ai/etherscan-exporter>
* ix.ai/ripple-exporter>
* ix.ai/stellar-exporter>
* ix.ai/grafana-email>

## Prerequisites:
### common.yml
* `Dockerfile` exists in the project root folder
* `README.md` exists in the project root folder
* the following variables are set on a project level:
  * `DOCKERHUB_USERNAME`
  * `DOCKERHUB_PASSWORD` - if this variable is not set (or is empty), all the images will be pushed only to the gitlab registry
  * `DOCKERHUB_REPO_PREFIX` - example: `ixdotai`
  * `DOCKERHUB_REPO_NAME` - example: `cioban`
* **Optional**: `build.sh` in the project's root folder to be executed before the actual build, as part of `script` in the `build:dev` and `build:release` jobs

### project-python.yml
* `.pylintrc` exists in the project root folder
* all the python files have the extension `.py`

### project-bash.yml
* all the bash scripts have the extension `.sh`

## Usage
In your `.gitlab-ci.yml` file add:
```yml
variables:
  DOCKERHUB_REPO_PREFIX: YOUR-DOCKER-HUB-REPO-PREFIX
  DOCKERHUB_REPO_NAME: YOUR-DOCKER-HUB-REPO-NAME

include:
  - remote: https://gitlab.com/ix.ai/ci-templates/raw/master/python-project.yml
```
> **Warning**: Make sure that you add `DOCKERHUB_USERNAME` and `DOCKERHUB_PASSWORD` as a variable [via the UI](https://docs.gitlab.com/ce/ci/variables/#via-the-ui) and you [mask](https://docs.gitlab.com/ce/ci/variables/#masked-variables) it.
