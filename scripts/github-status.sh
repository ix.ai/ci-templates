#!/bin/sh

set -e

if [ -z "$GITHUB_STATUS_TOKEN" ] || [ -z "$GITHUB_OWNER" ] || [ ! "${GITHUB_STATUS_ENABLE:-false}" = "true" ]; then
  echo "GitHub Status is disabled"
  exit 0
fi

if [ -z "${BUILD_STATUS}" ]; then
  echo "BUILD_STATUS is missing. Exiting."
  exit 1
fi

if [ -z "${CI_JOB_URL}" ]; then
  echo "CI_JOB_URL is missing. Exiting."
  exit 1
fi

if [ -z "${CI_JOB_NAME}" ]; then
  echo "CI_JOB_NAME is missing. Exiting."
  exit 1
fi

if [ -z "${CI_PROJECT_NAME}" ]; then
  echo "CI_PROJECT_NAME is missing. Exiting."
  exit 1
fi

if [ -z "${CI_COMMIT_SHA}" ]; then
  echo "CI_COMMIT_SHA is missing. Exiting."
  exit 1
fi

command -v curl || apk add --no-cache curl || (apt-get update && apt-get -y install --no-install-recommends curl)

echo "Updating GitHub Status to ${BUILD_STATUS}"
curl -so /dev/null \
      -XPOST -H "Authorization: token ${GITHUB_STATUS_TOKEN}" \
      -d "{\"state\": \"${BUILD_STATUS}\",\"target_url\": \"${CI_JOB_URL}\",\"context\": \"ci/gitlab/job/${CI_JOB_NAME}\",\"description\": \"${CI_JOB_NAME} ${BUILD_STATUS}.\"}" \
      "https://api.github.com/repos/${GITHUB_OWNER}/${CI_PROJECT_NAME}/statuses/${CI_COMMIT_SHA}"
exit $?
