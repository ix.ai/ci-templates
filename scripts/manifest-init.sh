#!/bin/sh

set -e

FOLDER='/kaniko/images'

mkdir -p "${FOLDER}"

ENABLE_DOCKER='false'
ENABLE_GITHUB='false'
ENABLE_GITLAB="${ENABLE_GITLAB_REGISTRY:-false}"

IMAGE=''

if [ -n "${DOCKERHUB_REPO_NAME}" ] && [ -n "${DOCKERHUB_PASSWORD}" ]; then
    ENABLE_DOCKER='true'
fi

if [ -n "${GITHUB_USERNAME}" ] && [ -n "${GITHUB_STATUS_TOKEN}" ] && [ -n "${GITHUB_REPO_NAME}" ]; then \
    ENABLE_GITHUB='true'
fi

# $1 - IMAGE
# $2 - platform
# $3 - tag
# $4 - latest
_create_file() {
    # set a default
    TAG="dev-branch"
    # the commit is actually on the default branch
    if [ "${CI_COMMIT_REF_NAME}" = "${CI_DEFAULT_BRANCH}" ]; then TAG="dev-${CI_DEFAULT_BRANCH}"; fi
    # the commit is on a tag
    if [ -n "${CI_COMMIT_TAG}" ]; then TAG="${CI_COMMIT_TAG}"; fi
    # `latest`
    if [ -n "${3}" ]; then TAG="${3}"; fi

    FILE="${FOLDER}/${2}-${TAG}.yml"

    cat <<xEOF > "${FILE}"
image: ${1}:${TAG}
manifests:
xEOF

    # now remove the `latest` TAG for the sources
    if [ "${TAG}" = "latest" ]; then TAG="${CI_COMMIT_TAG}"; fi

    if [ "${ENABLE_AMD64:-}" = "true" ]; then
        cat <<xEOF >> "${FILE}"
  -
    image: ${1}:${TAG}-amd64
    platform:
      architecture: amd64
      os: linux
xEOF
    fi
    if [ "${ENABLE_ARM64:-x}" = "true" ]; then
        cat <<xEOF >> "${FILE}"
  -
    image: ${1}:${TAG}-arm64
    platform:
      architecture: arm64
      os: linux
      variant: v8
xEOF
    fi
    if [ "${ENABLE_ARMv7:-x}" = "true" ]; then
        cat <<xEOF >> "${FILE}"
  -
    image: ${1}:${TAG}-armv7
    platform:
      architecture: arm
      os: linux
      variant: v7
xEOF
    fi
    if [ "${ENABLE_ARMv6:-x}" = "true" ]; then
        cat <<xEOF >> "${FILE}"
  -
    image: ${1}:${TAG}-armv6
    platform:
      architecture: arm
      os: linux
      variant: v6
xEOF
    fi
    if [ "${ENABLE_386:-x}" = "true" ]; then
        cat <<xEOF >> "${FILE}"
  -
    image: ${1}:${TAG}-386
    platform:
      architecture: 386
      os: linux
xEOF
    fi

}

# A commit to the tag always updates :latest
# So we need to potentially create six files:
# gitlab-${TAG}.yml
# gitlab-latest.yml
# dockerhub-${TAG}.yml
# dockerhub-latest.yml
# github-${TAG}.yml
# github-latest.yml
if [ "${ENABLE_GITLAB}" = 'true' ]; then
    IMAGE="${CI_REGISTRY_IMAGE}"
    _create_file "${IMAGE}" gitlab
    if [ -n "${CI_COMMIT_TAG}" ]; then
        _create_file "${IMAGE}" gitlab latest
    fi
fi
if [ "${ENABLE_DOCKER}" = 'true' ]; then
    IMAGE="${DOCKERHUB_REPO_PREFIX}/${DOCKERHUB_REPO_NAME}"
    _create_file "${IMAGE}" dockerhub
    if [ -n "${CI_COMMIT_TAG}" ]; then
        _create_file "${IMAGE}" dockerhub latest
    fi
fi

if [ "${ENABLE_GITHUB}" = 'true' ]; then
    IMAGE="ghcr.io/${GITHUB_REPO_NAME}"
    _create_file "${IMAGE}" github
    if [ -n "${CI_COMMIT_TAG}" ]; then
        _create_file "${IMAGE}" github latest
    fi
fi
