#!/bin/sh

set -e

TAG="${TAG:-}"

if [ -z "${TAG}" ]; then
    TAG="dev-branch${TAG_SUFFIX:-}"

    if [ "${CI_COMMIT_REF_NAME}" = "${CI_DEFAULT_BRANCH}" ]; then
        TAG="dev-${CI_DEFAULT_BRANCH}${TAG_SUFFIX:-}"
    fi

    if [ -n "${CI_COMMIT_TAG}" ]; then
        TAG="${CI_COMMIT_TAG}${TAG_SUFFIX:-}"
    fi
fi

KANIKO_ARGS="${KANIKO_ARGS} --context ${CI_PROJECT_DIR} --dockerfile ${CI_PROJECT_DIR}/Dockerfile --cleanup"

if [ "${ENABLE_GITLAB_REGISTRY}" = "true" ]; then
    KANIKO_ARGS="${KANIKO_ARGS} --destination=${CI_REGISTRY_IMAGE}:${TAG}"
fi

if [ -n "${DOCKERHUB_REPO_NAME}" ] && [ -n "${DOCKERHUB_PASSWORD}" ]; then
    KANIKO_ARGS="${KANIKO_ARGS} --destination=${DOCKERHUB_REPO_PREFIX}/${DOCKERHUB_REPO_NAME}:${TAG}"
fi

if [ -n "${GITHUB_USERNAME}" ] && [ -n "${GITHUB_STATUS_TOKEN}" ] && [ -n "${GITHUB_REPO_NAME}" ]; then
    KANIKO_ARGS="${KANIKO_ARGS} --destination=ghcr.io/${GITHUB_REPO_NAME}:${TAG}"
fi

if [ "${ENABLE_CACHE}" = "true" ]; then
    KANIKO_ARGS="${KANIKO_ARGS} --cache"
    if [ -n "${GITHUB_USERNAME}" ] && [ -n "${GITHUB_STATUS_TOKEN}" ] && [ -n "${GITHUB_REPO_NAME}" ]; then
        KANIKO_ARGS="${KANIKO_ARGS} --cache-repo ghcr.io/${GITHUB_REPO_NAME}"
    elif [ -n "${DOCKERHUB_REPO_NAME}" ] && [ -n "${DOCKERHUB_PASSWORD}" ]; then
        KANIKO_ARGS="${KANIKO_ARGS} --cache-repo ${DOCKERHUB_REPO_PREFIX}/${DOCKERHUB_REPO_NAME}"
    else
        KANIKO_ARGS="${KANIKO_ARGS} --cache-repo ${CI_REGISTRY_IMAGE}"
    fi
fi

# For visiblity, this is split here on multiple stand-alone lines
KANIKO_ARGS="${KANIKO_ARGS} --build-arg CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME}"
KANIKO_ARGS="${KANIKO_ARGS} --build-arg CI_PIPELINE_ID=${CI_PIPELINE_ID}"
KANIKO_ARGS="${KANIKO_ARGS} --build-arg CUSTOM_VERSION=${CUSTOM_VERSION:-none}"

if [ -n "${CI_COMMIT_TAG}" ]; then
    KANIKO_ARGS="${KANIKO_ARGS} --build-arg LATEST=true"
fi

if [ ! "${ADDITIONAL_KANIKO_ARGS:-}x" = "x" ]; then
    KANIKO_ARGS="${KANIKO_ARGS} ${ADDITIONAL_KANIKO_ARGS}"
fi

set -x
# shellcheck disable=SC2086
exec /kaniko/executor ${KANIKO_ARGS}
